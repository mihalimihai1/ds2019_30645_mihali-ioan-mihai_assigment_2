package com.example.springdemo.rabbitmq;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Consumer implements MessageListener {

    @Autowired
    private DataRepository dataRepository;

    @Override
    @RabbitListener(queues = "MyRabbitQueue2")
    public void onMessage(Message message) {
        String myMessage = new String(message.getBody());

        Mesaj mesaj = new Mesaj();
        mesaj.setActivity((Gson().fromJson(myMessage, JsonObject.class).get("activity")).toString());
        mesaj.setStart_time(Gson().fromJson(myMessage, JsonObject.class).get("start time").toString());
        mesaj.setEnd_time(Gson().fromJson(myMessage, JsonObject.class).get("end time").toString());
        mesaj.setId_patient(Gson().fromJson(myMessage, JsonObject.class).get("id patient").toString());
//        dataRepository.save(data);
       System.out.println("Received message = " + data.toString());
    }
}
