package com.example.springdemo.rabbitmq;

import javax.persistence.*;

@Entity
@Table(name = "mesaj")
public class Mesaj {
    @Id
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "startTime", unique = false, nullable = false)
    private String start_time;

    @Column(name = "endTime", unique = false, nullable = false)
    private String end_time;

    @Column(name = "activity", unique = false, nullable = false)
    private String activity;

    @Column(name = "idPatient", unique = false, nullable = false)
    private Integer id_patient;

    public Mesaj() {
    }

    public Mesaj(String start_time, String end_time, String activity, Integer id_patient) {
        this.start_time = start_time;
        this.end_time = end_time;
        this.activity = activity;
        this.id_patient = id_patient;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public Integer getId_patient() {
        return id_patient;
    }

    public void setId_patient(Integer id_patient) {
        this.id_patient = id_patient;
    }
}
