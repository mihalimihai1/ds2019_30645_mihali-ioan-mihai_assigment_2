package com.example.springdemo.rabbitmq;

import org.json.simple.JSONObject;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Producer {
    @Value("MyRabbitQueue2")
    private String queueName;

    @Value("Rabbit.In2")
    private String exchange;

    @Value("MY_ROUTING_KEY2")
    private String routingkey;

    @Autowired
    private AmqpTemplate amqpTemplate;

    ReadFromFile readFromFile = new ReadFromFile();

    public void getMessage(){
		try{
			readFromFile.read();
        for (JSONObject mydata : readFromFile.myData) {
            amqpTemplate.convertAndSend(exchange, routingkey, mydata);
            Thread.sleep(500);
        }
	  }
	  catch(Exception e){
			System.out.println("Something went wrong.");
		  
	  }
        
    }
}
